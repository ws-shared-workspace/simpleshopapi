<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use App\Factory\UserFactory;
use App\Test\ExtendedApiTestCase;
use App\Factory\ProductFactory;

class BasketTest extends ExtendedApiTestCase
{
    public static function basketInitializationWithSuccessDataProvider(): iterable
    {
        yield [UserFactory::new(['email'=>'user1@webskigosc.com'])];
        yield [UserFactory::new(['email'=>'user2@webskigosc.com'])];
    }

    /**
     * @dataProvider basketInitializationWithSuccessDataProvider
     */
    public function testBasketInitializationWithSuccess(UserFactory $userFixtures): void
    {
        /* GIVEN */
        $client = static::createClient();
        $user = $userFixtures->create();
        $token = $this->logIn($client, $user->getEmail(), '1234');

        $options = [
            'headers' => [
                'Authorization' => "Bearer $token",
                'Content-Type' => 'application/json',
            ],
        ];

        $data = [
            'json' => [],
        ];

        /* WHEN */
        $response = $client->request('POST', '/api/baskets/basket_init', array_merge($options, $data));

        /* THEN */
        $this->assertResponseIsSuccessful();
        $this->assertEquals(201, $response->getStatusCode());
    }

    public static function basketInitializationShouldSupportJustOneBasketDataProvider(): iterable
    {
        yield [UserFactory::new(['email'=>'user1@webskigosc.com'])];
        yield [UserFactory::new(['email'=>'user2@webskigosc.com'])];
    }

    /**
     * @dataProvider basketInitializationShouldSupportJustOneBasketDataProvider
     */
    public function testBasketInitializationShouldSupportJustOneBasket(UserFactory $userFixtures): void
    {
        /* GIVEN */
        $client = static::createClient();
        $user = $userFixtures->create();
        $token = $this->logIn($client, $user->getEmail(), '1234');

        $options = [
            'headers' => [
                'Authorization' => "Bearer $token",
                'Content-Type' => 'application/json',
            ],
        ];

        $data = [
            'json' => [],
        ];

        /* WHEN */
        $response = $client->request('POST', '/api/baskets/basket_init', array_merge($options, $data));
        $basket = json_decode($response->getContent(), true);

        $response2 = $client->request('POST', '/api/baskets/basket_init', array_merge($options, $data));

        /* THEN */
        $this->assertResponseIsSuccessful();
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertResponseIsSuccessful();
        $this->assertEquals(201, $response2->getStatusCode());

        $this->assertEquals('1', $user->getBaskets()->count());
        $this->assertEquals($basket['id'], $user->getBaskets()->last()->getId());
    }

    public function testBasketInitializationByAnonymousUserShouldFail(): void
    {
        /* GIVEN */
        $client = static::createClient();

        $data = [
            'json' => [],
        ];

        /* WHEN */
        $response = $client->request('POST', '/api/baskets/basket_init', $data);

        /* THEN */
        $this->assertEquals(401, $response->getStatusCode());
    }
}
