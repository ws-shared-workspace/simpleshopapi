<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use App\Test\ExtendedApiTestCase;
use App\Factory\ProductFactory;

class ProductTest extends ExtendedApiTestCase
{
    /**
     * @dataProvider productDataProvider
     */
    public function testGetAllProductsWithSuccess(ProductFactory $productFixtures): void
    {
        $client = static::createClient();
        $product = $productFixtures->create();

        $response = $client->request('GET', '/api/products', []);

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @dataProvider productDataProvider
     */
    public function testGetProductByIdWithSuccess(ProductFactory $productFixtures): void
    {
        $client = static::createClient();
        $product = $productFixtures->create();

        $response = $client->request('GET', '/api/products/'.$product->getId(), []);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public static function productDataProvider(): iterable
    {
        yield [ProductFactory::new()];
        yield [ProductFactory::new()];
        yield [ProductFactory::new()];
    }
}
