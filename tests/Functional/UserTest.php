<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use App\Test\ExtendedApiTestCase;
use App\Factory\UserFactory;

class UserTest extends ExtendedApiTestCase
{
    /**
     * @dataProvider userDataProvider
     */
    public function testReceiveAuthenticationTokenWithSuccess(UserFactory $userFromDatabase): void
    {
        $client = static::createClient();
        $user = $userFromDatabase->create();

        $response = $client->request('POST', '/authentication_token', [
            'json' => [
                'email' => $user->getEmail(),
                'password' => '1234'
            ],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('token', json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR));
    }

    public static function userDataProvider(): iterable
    {
        yield [UserFactory::new(['email'=>'administrator@webskigosc.com'])];
    }
}
