<?php

namespace App\Entity;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Basket\BasketInitializationController;
use App\Entity\Trait\Timestamp;
use App\Repository\BasketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BasketRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['controller' => NotFoundAction::class,'read' => false,'output' => false],
        'post' => ['controller' => NotFoundAction::class,'read' => false,'output' => false],
        'basket_init' => [
            'method' => 'POST',
            'path' => '/baskets/basket_init',
            'controller' => BasketInitializationController::class,
        ],
    ],
    itemOperations: [
        'get' => ['controller' => NotFoundAction::class,'read' => false,'output' => false],
        'put' => ['controller' => NotFoundAction::class,'read' => false,'output' => false],
        'patch' => ['controller' => NotFoundAction::class,'read' => false,'output' => false],
        'delete' => ['controller' => NotFoundAction::class,'read' => false,'output' => false],
    ],
    security: "is_granted('ROLE_USER')",
)]

class Basket
{
    use Timestamp;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=BasketItem::class, mappedBy="basket")
     */
    private $basketItems;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="baskets")
     */
    private $user;

    public function __construct()
    {
        $this->basketItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|BasketItem[]
     */
    public function getBasketItems(): Collection
    {
        return $this->basketItems;
    }

    public function addBasketItem(BasketItem $basketItem): self
    {
        if (!$this->basketItems->contains($basketItem)) {
            $this->basketItems[] = $basketItem;
            $basketItem->setBasket($this);
        }

        return $this;
    }

    public function removeBasketItem(BasketItem $basketItem): self
    {
        if ($this->basketItems->removeElement($basketItem)) {
            // set the owning side to null (unless already changed)
            if ($basketItem->getBasket() === $this) {
                $basketItem->setBasket(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
