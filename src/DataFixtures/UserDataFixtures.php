<?php

namespace App\DataFixtures;

use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class UserDataFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $admin = UserFactory::createOne([
            'email'=>'administrator@webskigosc.com',
            'roles' => ['ROLE_USER', 'ROLE_ADMIN']
        ]);
        $user1 = UserFactory::createOne([
            'email'=>'user1@webskigosc.com',
            'roles' => ['ROLE_USER']
        ]);
        $user2 = UserFactory::createOne([
            'email'=>'user2@webskigosc.com',
            'roles' => ['ROLE_USER']
        ]);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['dev'];
    }
}
