<?php

namespace App\Test;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ExtendedApiTestCase extends ApiTestCase {

    use ResetDatabase, Factories;

    protected function logIn(Client $client, string $email, string $password): string
    {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ];

        $response = $client->request('POST', '/authentication_token', [
            'json' => [
                'email' => $email,
                'password' => $password,
            ],
        ], $options);

        $this->assertResponseStatusCodeSame(200);

        return json_decode($response->getContent(), true)['token'];
    }

}
