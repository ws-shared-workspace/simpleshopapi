<?php declare(strict_types=1);


namespace App\Controller\Basket;


use App\Entity\User;
use App\Entity\Basket;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;

class BasketInitializationHandler
{

    private Security $security;

    private ?User $user;

    public function __construct(Security $security)
    {
        $this->security = $security;
        $this->user = $this->security->getUser();

        if (null === $this->user) {
            throw new AccessDeniedHttpException('Login required.');
        }
    }

    public function handle(Basket $data): void {
        $data->setCreatedAt();
        $data->setUpdatedAt();
        $data->setUser($this->user);
    }

}