<?php declare(strict_types=1);

namespace App\Controller\Basket;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use App\Entity\Basket;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;

#[AsController]
class BasketInitializationController extends AbstractController
{
    private BasketInitializationHandler $basketInitializationHandler;
    private ?User $user;

    public function __construct(BasketInitializationHandler $basketInitializationHandler, Security $security)
    {
        $this->basketInitializationHandler = $basketInitializationHandler;

        $this->security = $security;
        $this->user = $this->security->getUser();

        if (null === $this->user) {
            throw new AccessDeniedHttpException('Login required.');
        }
    }

    public function __invoke(Basket $data): Basket
    {
        // Allow just one basket per user
        $userBaskets = $this->user->getBaskets();
        if($userBaskets->count() === 0) {
            $this->basketInitializationHandler->handle($data);
            return $data;
        }

        return $userBaskets->last();
    }

}