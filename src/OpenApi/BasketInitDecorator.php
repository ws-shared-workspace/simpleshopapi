<?php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

final class BasketInitDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['BasketInitResponse']       = new \ArrayObject([
            'type'       => 'object',
            'properties' => [
                'token' => [
                    'type'     => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);
        $schemas['BasketInitRequest'] = new \ArrayObject([
            'type'       => 'object',
            'properties' => [],
        ]);

        $pathItem = new Model\PathItem(
            ref: 'Basket initialization',
            post: new Model\Operation(
                operationId: 'postBasketInitialize',
                tags: ['Basket'],
                responses: [
                    '200' => [
                        'description' => 'API should return your Basket object.',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/BasketInitResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'To do something with basket, you have initialize your own Basket first.',
                requestBody: new Model\RequestBody(
                    description: 'You don\'t have to define any request body.',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/BasketInitRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/api/baskets/basket_init', $pathItem);

        return $openApi;
    }
}